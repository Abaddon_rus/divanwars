﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class Extension
{
    public static T Find<T>(this ICollection<T> source, Predicate<T> predicate)
    {
        T result = default(T);
        foreach (var item in source)
        {
            if (predicate(item))
            {
                result = item;
                break;
            }
        }

        return result;
    }

    public static T Clone<T>(this T origin)
    {
        string json = JsonUtility.ToJson(origin);
        return JsonUtility.FromJson<T>(json);
    }
}

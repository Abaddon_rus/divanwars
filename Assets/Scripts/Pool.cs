﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Pool
{
    static Dictionary<Entity, List<Entity>> PoolEntity = new Dictionary<Entity, List<Entity>>();

    public static void AddInPool(Entity prefab, Entity entity)
    {
        List<Entity> entitys;
        if (!PoolEntity.TryGetValue(prefab, out entitys))
        {
            entitys = new List<Entity>();
            PoolEntity.Add(prefab, entitys);
        }

        entitys.Add(entity);
    }

    public static T GetFromPool<T>(Entity prefab) where T : Entity
    {
        List<Entity> entitys;
        Entity entity;

        if (PoolEntity.TryGetValue(prefab, out entitys) && entitys.Count > 0)
        {
            entity = entitys[entitys.Count - 1];
            entitys.RemoveAt(entitys.Count - 1);
            return entity as T;
        }

        return Object.Instantiate(prefab) as T;
    }

    public static void ClearPool()
    {
        PoolEntity = new Dictionary<Entity, List<Entity>>();
    }
}

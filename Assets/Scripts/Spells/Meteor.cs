﻿using System.Collections.Generic;
using UnityEngine;

public class Meteor : SpellEffect
{
    List<Enemy> _enemies = new List<Enemy>();

    private void Start()
    {
        Position();
    }

    private void Update()
    {
        Position();
        if (Input.GetMouseButtonDown(0))
        {
            Activate();
        }
        if (Input.GetMouseButtonDown(1))
        {
            Cancel();
        }
    }

    void Position()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 1000, Using))
        {
            transform.position = hit.point;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Enemy enemy = other.GetComponent<Enemy>();
        _enemies.Add(enemy);
        enemy.GetComponent<Renderer>().material.color = Color.green;
    }

    private void OnTriggerExit(Collider other)
    {
        Enemy enemy = other.GetComponent<Enemy>();
        _enemies.Remove(enemy);
        enemy.GetComponent<Renderer>().material.color = Color.red;
    }

    public override void Begin()
    {
        transform.localScale = new Vector3(SpellParams.Radius, SpellParams.Radius, SpellParams.Radius);
        gameObject.layer = 15;
    }

    public override void Activate()
    {
        base.Activate();
        for (int i = 0; i < _enemies.Count; i++)
        {
            _enemies[i].Damage(SpellParams.Damage);
        }
        Cancel();
    }

    public override void Cancel()
    {
        for (int i = 0; i < _enemies.Count; i++)
        {
            _enemies[i].GetComponent<Renderer>().material.color = Color.red;
        }
        Destroy(gameObject);
        ManualController.UseSpells = false;
    }
}

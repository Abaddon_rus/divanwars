﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : SpellEffect
{
    Enemy _enemy;

    [SerializeField]
    LineRenderer _lineRenderer;

    Hero _hero;

    private void Start()
    {
        _hero = Core.GetLink<Hero>();
        Position();
    }

    private void Update()
    {
        Position();
        if (Input.GetMouseButtonDown(0))
        {
            Activate();
        }
        if (Input.GetMouseButtonDown(1))
        {
            Cancel();
        }
    }

    void Position()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 1000, Using))
        {
            Enemy enemy = hit.transform.GetComponentInParent<Enemy>();
            if (enemy != null)
            {
                _enemy = enemy;

                _enemy.GetComponent<Renderer>().material.color = Color.green;
            }
            else
            {
                if (_enemy != null)
                {
                    _enemy.GetComponent<Renderer>().material.color = Color.red;
                }
                _enemy = null;
            }
            transform.position = hit.point;
        }

        _lineRenderer.SetPositions(new Vector3[] { _hero.transform.position, transform.position });
    }

    public override void Begin()
    {

    }

    public override void Activate()
    {
        if (_enemy != null)
        {
            base.Activate();
            _enemy.Damage(SpellParams.Damage);
            Cancel();
        }
    }

    public override void Cancel()
    {
        if (_enemy != null)
        {
            _enemy.GetComponent<Renderer>().material.color = Color.red;
        }
        Destroy(gameObject);
        ManualController.UseSpells = false;
    }
}

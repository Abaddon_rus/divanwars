﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ManualController : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    List<Entity> Selected = new List<Entity>();

    [SerializeField]
    LayerMask _layerMaskAttack;

    [SerializeField]
    LayerMask _layerMaskSelect;

    [SerializeField]
    LineRenderer _lineRenderer;

    Vector3 _firstPont = new Vector3();
    Vector3 _secondPoint = new Vector3();

    public static bool UseSpells = false;
    public static bool UIFocus = false;

    private void Awake()
    {
        Core.AddLink(this);
    }

    private void Update()
    {
        if (!UseSpells && !UIFocus)
        {
            if (Input.GetMouseButtonDown(1))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit, 1000, _layerMaskAttack))
                {
                    Entity entity = hit.transform.GetComponent<Entity>();
                    if (entity != null)
                    {
                        for (int i = 0; i < Selected.Count; i++)
                        {
                            Selected[i].ManualAttack(entity);
                        }
                    }
                    else
                    {
                        Vector3 point = hit.point;
                        Hero hero = Selected.Find((x) => x as Hero) as Hero;
                        if (hero != null)
                        {
                            hero.ManualMove(point);
                        }
                    }

                    for (int i = 0; i < Selected.Count; i++)
                    {
                        Selected[i].OnDie += DieSelectedUnit;
                    }
                }
            }
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit, 1000, _layerMaskSelect))
                {
                    Entity entity = hit.transform.GetComponent<Entity>();
                    if (entity != null)
                    {
                        Unselect();
                        Selected.Add(entity);
                        entity.Select();
                    }
                    else
                    {
                        Unselect();
                    }
                }
            }
        }
    }

    void Unselect()
    {
        for (int i = 0; i < Selected.Count; i++)
        {
            Selected[i].Unselect();
            Selected[i].OnDie -= DieSelectedUnit;
        }
        Selected.Clear();
    }

    void DieSelectedUnit(Entity entity)
    {
        entity.OnDie -= DieSelectedUnit;
        Selected.Remove(entity);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 1000, _layerMaskSelect))
            {
                _firstPont = hit.point;
            }

            _lineRenderer.enabled = true;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;
            Vector3 leftBot;
            Vector3 rightTop;
            Vector3 leftTop;
            Vector3 rightBot;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 1000, _layerMaskSelect))
            {
                _secondPoint = hit.point;
            }

            leftBot = new Vector3(Mathf.Min(_firstPont.x, _secondPoint.x), 0.01f, Mathf.Min(_firstPont.z, _secondPoint.z));
            rightTop = new Vector3(Mathf.Max(_firstPont.x, _secondPoint.x), 0.01f, Mathf.Max(_firstPont.z, _secondPoint.z));
            leftTop = new Vector3(Mathf.Min(_firstPont.x, _secondPoint.x), 0.01f, Mathf.Max(_firstPont.z, _secondPoint.z));
            rightBot = new Vector3(Mathf.Max(_firstPont.x, _secondPoint.x), 0.01f, Mathf.Min(_firstPont.z, _secondPoint.z));


            Vector3[] vector3s = new[] { leftTop, rightTop, rightBot, leftBot };

            _lineRenderer.SetPositions(vector3s);
        }
    }

    public IEnumerator CooldownSpells(SpellSettings spell)
    {
        float sec = 0;
        while (spell.Cooldown > 0)
        {
            yield return null;
            sec += Time.deltaTime;
            while (sec >= 1)
            {
                sec--;
                spell.Cooldown--;
            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        RaycastHit hit;
        Vector3 min = new Vector3();
        Vector3 max = new Vector3();

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 1000, _layerMaskSelect))
        {
            _secondPoint = hit.point;
        }

        min = new Vector3(Mathf.Min(_firstPont.x, _secondPoint.x), 0, Mathf.Min(_firstPont.z, _secondPoint.z));
        max = new Vector3(Mathf.Max(_firstPont.x, _secondPoint.x), 0, Mathf.Max(_firstPont.z, _secondPoint.z));
        for (int i = 0; i < Core.Minion.Count; i++)
        {
            Vector3 pos = Core.Minion[i].transform.position;
            if (pos.x > min.x && pos.z > min.z)
            {
                if (pos.x < max.x && pos.z < max.z)
                {
                    Selected.Add(Core.Minion[i]);
                    Core.Minion[i].Select();
                }
            }
        }

        _lineRenderer.enabled = false;
    }
}
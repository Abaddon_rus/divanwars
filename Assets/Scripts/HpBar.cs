﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HpBar : MonoBehaviour
{
    [SerializeField]
    SpriteRenderer _fillHp;

    public void ChangeHp(float percent)
    {
        _fillHp.transform.localPosition = new Vector3(-(1 - percent) * 1.5f, 0, -0.01f);
        _fillHp.transform.localScale = new Vector3(percent, 0.4f, 0);
    }

    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, Camera.main.transform.position, 0);
        transform.LookAt(transform.position + Camera.main.transform.rotation * Vector3.forward, Camera.main.transform.rotation * Vector3.up);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(HeroSettings))]
public class HeroSettingsEditor : Editor
{
    HeroSettings _target;

    List<bool> _visebleList;

    bool _listVisible = false;

    private void OnEnable()
    {
        _target = target as HeroSettings;
        int count = _target.LevelUpParams.Count;
        _visebleList = new List<bool>(count);
        for (int i = 0; i < count; i++)
        {
            _visebleList.Add(false);
        }
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        List<LevelUpParams> itemsArray = _target.LevelUpParams;

        _listVisible = EditorGUILayout.Foldout(_listVisible, "Параметры повышения уровня");
        if (_listVisible)
        {
            DrawItemCounter(itemsArray);
            for (int i = 0; i < itemsArray.Count; i++)
            {
                DrawItem(itemsArray[i], i);
            }
        }
    }

    void DrawItemCounter(List<LevelUpParams> itemsArray)
    {
        int count = EditorGUILayout.DelayedIntField("Кол-во уровней: ", itemsArray.Count);
        if (count != itemsArray.Count)
        {
            if (count < itemsArray.Count)
            {
                itemsArray.RemoveRange(count, itemsArray.Count - count);
                _visebleList.RemoveRange(count, itemsArray.Count - count);
            }
            else
            {
                for (int i = itemsArray.Count; i < count; i++)
                {
                    itemsArray.Add(new LevelUpParams());
                    _visebleList.Add(false);
                }
            }
        }
    }

    void DrawItem(BaseCharacteristics item, int index)
    {
        EditorGUI.indentLevel++;
        _visebleList[index] = EditorGUILayout.Foldout(_visebleList[index], "Уровень: " + (index + 2));
        if (_visebleList[index])
        {
            EditorGUI.indentLevel++;
            LevelUpParams lvlParams = _target.LevelUpParams[index];
            lvlParams.NeedExp = EditorGUILayout.DelayedIntField("Tребуется ОП: ", lvlParams.NeedExp);
            lvlParams.MaxHP = EditorGUILayout.DelayedIntField("ХП: ", lvlParams.MaxHP);
            lvlParams.Armor = EditorGUILayout.DelayedIntField("Броня: ", lvlParams.Armor);
            lvlParams.Attack = EditorGUILayout.DelayedIntField("Урон: ", lvlParams.Attack);
            lvlParams.AttackSpeed = EditorGUILayout.DelayedFloatField("Скорость атаки: ", lvlParams.AttackSpeed);
            lvlParams.AttackRange = EditorGUILayout.DelayedIntField("Дальность атаки: ", lvlParams.AttackRange);
            lvlParams.Speed = EditorGUILayout.DelayedIntField("Скорость: ", lvlParams.Speed);
            EditorGUI.indentLevel--;
        }

        EditorGUI.indentLevel--;

        EditorUtility.SetDirty(_target);
    }
}

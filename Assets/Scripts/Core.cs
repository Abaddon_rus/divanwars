﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Core
{
    static Dictionary<string, object> Links = new Dictionary<string, object>();

    public static Transform Divan;

    public static List<Entity> Minion = new List<Entity>();

    public static List<Entity> Enemy = new List<Entity>();

    public static event Action OnChangeCountEnemy = delegate { };

    public static event Action OnChangeCountMinions = delegate { };

    public static bool Stop = false;

    public static void AddEnemy(Entity unit)
    {
        Enemy.Add(unit);
        OnChangeCountEnemy();
    }

    public static void RemoveEnemy(Entity unit)
    {
        Enemy.Remove(unit);
    }

    public static void AddMinion(Entity unit)
    {
        Minion.Add(unit);
        OnChangeCountMinions();
    }

    public static void RemoveMinion(Entity unit)
    {
        Minion.Remove(unit);
    }

    public static void ClearPool()
    {
        Links.Clear();
        Minion = new List<Entity>();
        Enemy = new List<Entity>();
        OnChangeCountEnemy = delegate { };
        OnChangeCountMinions = delegate { };
    }

    public static void AddLink(object classObject)
    {
        Links.Add(classObject.GetType().ToString(), classObject);
    }

    public static T GetLink<T>()
    {
        T result = default(T);
        Type typeParameterType = typeof(T);
        object value = Links.Values.Find((x) => x.GetType() == typeParameterType);
        result = (T)value;
        return result;
    }

    public static void RemoveLink(object classObject)
    {
        Links.Remove(classObject.GetType().ToString());
    }
}

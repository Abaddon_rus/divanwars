﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Hero : Unit
{
    public List<SpellSettings> _spells;

    public int FreePointSpell = 1;

    int _exp = 0;
    int _needExp = 0;

    public static Action<int> Exp;

    [HideInInspector]
    public HeroSettings _heroSettings;

    private void Start()
    {
        if (_heroSettings.Lvl <= _heroSettings.LevelUpParams.Count)
        {
            _needExp = _heroSettings.LevelUpParams[_heroSettings.Lvl - 1].NeedExp;
        }
        Exp = AddExp;
    }

    public override void Spawn()
    {
        base.Spawn();
        Core.AddMinion(this);
    }

    public override void SpecializedTargets()
    {
        _target = transform;
    }

    public void AddExp(int exp)
    {
        if (_heroSettings.Lvl <= _heroSettings.LevelUpParams.Count)
        {
            _exp += exp;
            if (_exp >= _needExp)
            {
                _exp -= _needExp;
                _heroSettings.Lvl++;
                FreePointSpell++;
                if (_heroSettings.Lvl <= _heroSettings.LevelUpParams.Count)
                {
                    _needExp = _heroSettings.LevelUpParams[_heroSettings.Lvl - 1].NeedExp;
                }
            }
        }

        HeroPanel.ExpLvl(GetPercentExp(), _heroSettings.Lvl);
    }

    public float GetPercentHp()
    {
        return (float)_hp / _characteristics.MaxHP;
    }

    public float GetPercentExp()
    {
        return (float)_exp / _needExp;
    }

    public override void Select()
    {
        base.Select();
        Core.RemoveLink(this);
        Core.AddLink(this);
        HeroPanel.Select(this);
        CameraController.SelectHero = this;
    }

    public override void Unselect()
    {
        base.Unselect();
        Core.RemoveLink(this);
        HeroPanel.Unselect();
        CameraController.SelectHero = null;
    }

    public override void Damage(int damage)
    {
        base.Damage(damage);
        HeroPanel.HP(GetPercentHp());
    }

    public override void Heal(int heal)
    {
        base.Heal(heal);
        HeroPanel.HP(GetPercentHp());
    }

    public override void ManualMove(Vector3 vector3)
    {
        _attackRange.OnEnter -= Attack;
        _eyesRange.OnEnter -= EyesEnter;
        _target = null;
        _manualTarget = null;
        _manualControl = true;
        _marker.transform.position = vector3;
        _manualMovePoint = vector3;
        _navMeshAgent.SetDestination(_manualMovePoint);
    }

    public override void ApplyСharacteristics()
    {
        _heroSettings = _settings as HeroSettings;
        _characteristics = _heroSettings.Characteristics;
    }

    public void StopManualMove()
    {
        _marker.transform.position = new Vector3(0, -10);
        _manualControl = false;
        FindTarget();
    }

    public override void Die()
    {
        base.Die();
        Core.RemoveMinion(this);
        DethHero();
    }

    void DethHero()
    {
        Core.GetLink<Fountain>().HeroDie();
    }
}

﻿public class Minion : Unit
{
    Fountain _fountain;

    public override void Spawn()
    {
        _fountain = Core.GetLink<Fountain>();
        base.Spawn();
        FindTarget();
        Core.AddMinion(this);
        Core.OnChangeCountEnemy += FindTarget;
    }

    public override void SpecializedTargets()
    {
        if (Core.Enemy.Count > 0)
        {
            _target = Core.Enemy[0].transform;
            Core.Enemy[0].OnDie += EnemyUnitDie;
        }
        else
        {
            _target = _fountain.transform;
            _eyesRange.OnEnter -= EyesEnter;
            _attackRange.OnEnter -= Attack;
        }
    }

    public override void Die()
    {
        Core.RemoveMinion(this);
        base.Die();
    }
}

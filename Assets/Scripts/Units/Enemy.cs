﻿public class Enemy : Unit
{
    public override void Spawn()
    {
        base.Spawn();
        FindTarget();
        Core.AddEnemy(this);
        Core.OnChangeCountMinions += FindTarget;
    }

    public override void SpecializedTargets()
    {
        if (Core.Minion.Count > 0)
        {
            _target = Core.Minion[0].transform;
            Core.Minion[0].OnDie += EnemyUnitDie;
        }
        else
        {
            _target = Core.Divan;
        }
    }

    public override void Attack(Entity entity)
    {
        if (entity as Unit)
        {
            base.Attack(entity);
        }
        else if (entity as Build)
        {
            if (Core.Minion.Count == 0)
            {
                base.Attack(entity);
            }
        }
    }

    public override void StopAttack(Entity entity)
    {
        if (entity as Unit)
        {
            base.StopAttack(entity);
        }
    }

    public override void Die()
    {
        Core.RemoveEnemy(this);
        Hero.Exp(_characteristics.Exp);
        Gold.Value += _characteristics.Gold;
        base.Die();
    }
}

﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class Unit : Entity
{
    [SerializeField]
    protected NavMeshAgent _navMeshAgent;

    [SerializeField]
    SphereCollider _rangeAttack;

    [SerializeField]
    HpBar _hpBar;

    [SerializeField]
    protected ScriptableObject _settings;

    protected Vector3 _manualMovePoint;

    protected Entity _eyesTarget;

    public Entity _tartgetAttack;

    [SerializeField]
    protected Transform _target = null;

    [SerializeField]
    protected RangeController _attackRange;

    [SerializeField]
    protected RangeController _eyesRange;

    protected Coroutine _coroutineAttack;

    private void Update()
    {
        if (!_manualControl && _target != null)
        {
            if (_target.transform != transform)
            {
                _navMeshAgent.SetDestination(_target.position);
                _marker.transform.position = _target.position;
            }
            if (_tartgetAttack != null)
            {
                _marker.transform.position = _tartgetAttack.transform.position;
            }
        }
        else if (_manualControl)
        {
            if (_manualTarget == null)
            {
                if (transform.position.x != _manualMovePoint.x && transform.position.z != _manualMovePoint.z)
                {
                    _navMeshAgent.SetDestination(_manualMovePoint);
                    _marker.transform.position = _manualMovePoint;
                }
                else
                {
                    _manualControl = false;
                }
            }
            else
            {
                _navMeshAgent.SetDestination(_manualTarget.transform.position);
                _marker.transform.position = _manualTarget.transform.position;
            }
        }
    }

    private void Awake()
    {
        _eyesRange.OnExit += EyesExit;
    }

    public override void Spawn()
    {
        base.Spawn();
        ApplyСharacteristics();
        _hp = _characteristics.MaxHP;
        _rangeAttack.radius = _characteristics.AttackRange;
        OnChangeHp += ChangeHpBar;
        gameObject.SetActive(true);
    }

    public virtual void ApplyСharacteristics()
    {
        _characteristics = (_settings as UnitSettings).Characteristics.Clone();
    }

    public void AddСharacteristics()
    {
        float percentHp = (float)_hp / _characteristics.MaxHP;
        _rangeAttack.radius = _characteristics.AttackRange;
        _hp = (int)Mathf.Round(_characteristics.MaxHP * percentHp);
        ChangeHpBar();
    }

    void ChangeHpBar()
    {
        _hpBar.ChangeHp((float)_hp / _characteristics.MaxHP);
    }

    public void FindTarget()
    {
        if (gameObject.activeInHierarchy)
        {
            _attackRange.OnEnter -= Attack;
            _eyesRange.OnEnter -= EyesEnter;
            if (!HaveManualTarget())
            {
                if (!HaveTargetAttack())
                {
                    _attackRange.OnEnter += Attack;
                    if (!HaveEyesTarget())
                    {
                        _eyesRange.OnEnter += EyesEnter;
                        SpecializedTargets();
                    }
                }
            }
        }
    }

    bool HaveManualTarget()
    {
        if (_manualTarget != null)
        {
            Entity entity = _attackRange.Entity.Find((x) => x == _attackRange);
            if (entity == null)
            {
                entity = _eyesRange.Entity.Find((x) => x == _attackRange);
                if (entity == null)
                {
                    _attackRange.OnEnter += Attack;
                    _eyesRange.OnEnter += EyesEnter;
                    _target = _manualTarget.transform;
                }
                else
                {
                    EyesEnter(entity);
                }
            }
            else
            {
                Attack(entity);
            }
        }
        return _manualTarget != null;
    }

    bool HaveTargetAttack()
    {
        _tartgetAttack = _attackRange.GetTarget<Unit>();
        if (_tartgetAttack != null)
        {
            Attack(_tartgetAttack);
        }
        return _tartgetAttack != null;
    }

    bool HaveEyesTarget()
    {
        _eyesTarget = _eyesRange.GetTarget<Unit>();
        if (_eyesTarget != null)
        {
            _target = _eyesTarget.transform;
        }
        return _eyesTarget != null;
    }

    public virtual void Attack(Entity entity)
    {
        if (entity == _manualTarget)
        {
            _manualControl = false;
        }

        if (!_manualControl)
        {
            _attackRange.OnEnter -= Attack;
            _attackRange.OnExit += StopAttack;

            _eyesTarget = null;
            _tartgetAttack = entity;
            _target = transform;
            _navMeshAgent.SetDestination(_target.position);
            _coroutineAttack = StartCoroutine(AttackUnit());
        }
    }

    public virtual void StopAttack(Entity entity)
    {
        if (_tartgetAttack == entity)
        {
            _attackRange.OnExit -= StopAttack;
            if (_coroutineAttack != null)
            {
                StopCoroutine(_coroutineAttack);
            }
            _tartgetAttack = null;
            _target = null;
            FindTarget();
        }
    }

    public void EyesEnter(Entity entity)
    {
        if (entity == _manualTarget)
        {
            _manualControl = false;
        }

        if (!_manualControl)
        {
            _eyesRange.OnEnter -= EyesEnter;
            entity.OnDie += EnemyUnitDie;
            if (_attackRange == null && _eyesTarget == null)
            {
                _eyesTarget = entity;
                _target = _eyesTarget.transform;
            }
        }
    }

    public void EyesExit(Entity entity)
    {
        if (_eyesTarget == entity)
        {
            if (entity._hp > 0)
            {
                entity.OnDie -= EnemyUnitDie;
            }

            _eyesTarget = null;
            FindTarget();
        }
    }

    protected IEnumerator AttackUnit()
    {
        while (_tartgetAttack != null && _tartgetAttack.gameObject.activeInHierarchy)
        {
            transform.LookAt(_tartgetAttack.transform);
            yield return new WaitForSeconds((1 / _characteristics.AttackSpeed) / 2);
            if (_tartgetAttack != null && _tartgetAttack.gameObject.activeInHierarchy)
            {
                _tartgetAttack.Damage((int)Mathf.Round(_characteristics.Attack * ((100f - _characteristics.Armor) / 100f)));
            }
            yield return new WaitForSeconds((1 / _characteristics.AttackSpeed) / 2);
        }
        StopAttack(_tartgetAttack);
    }

    public virtual void SpecializedTargets()
    {

    }

    public IEnumerator SpellEffect(SpellEffect spell)
    {
        SpellParams sp = spell.SpellParams;
        float sec = 0;
        float action = sp.TimeActionSec;

        _characteristics.AttackSpeed = _characteristics.AttackSpeed * (1 - sp.SlowdownAttack);
        _characteristics.Speed = (int)(_characteristics.Speed * (1 - sp.Slowdown));

        while (action > 0)
        {
            yield return null;
            sec += Time.deltaTime;
            while (sec >= 1)
            {
                sec--;
                action--;
            }
        }

        ApplyСharacteristics();
    }

    public override void ManualAttack(Entity entity)
    {
        if (_tartgetAttack != null)
        {
            StopAttack(_tartgetAttack);
        }
        _manualTarget = entity;
        _manualControl = true;
        FindTarget();
    }

    public override void Die()
    {
        StopAllCoroutines();
        _tartgetAttack = null;
        _eyesTarget = null;
        _target = null;
        _eyesRange.OnEnter -= EyesEnter;
        _attackRange.OnEnter -= Attack;
        _hp = _characteristics.MaxHP;
        _eyesRange.Clear();
        _attackRange.Clear();
        base.Die();
    }

    public void EnemyUnitDie(Entity entity)
    {
        StopAttack(entity);
        EyesExit(entity);
        entity.OnDie -= EnemyUnitDie;
        FindTarget();
    }
}
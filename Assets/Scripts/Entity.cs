﻿using System;
using UnityEngine;

public class Entity : MonoBehaviour
{
    Entity _prefab;

    [HideInInspector]
    public bool _mortal = true;

    [SerializeField]
    protected GameObject _marker;

    [SerializeField]
    GameObject _selectCircle;

    public int _hp = 10;

    [HideInInspector]
    public BaseCharacteristics _characteristics;

    public event Action<Entity> OnDie = delegate { };

    public event Action OnChangeHp = delegate { };

    [HideInInspector]
    public bool _manualControl = false;

    [HideInInspector]
    public Entity _manualTarget;

    public virtual void Die()
    {
        OnDie(this);
        AddInPool();
    }

    public virtual void Damage(int damage)
    {
        _hp -= damage;
        if (_hp <= 0)
        {
            Die();
        }
        OnChangeHp();
    }

    public virtual void Heal(int heal)
    {
        _hp = Mathf.Clamp(_hp + heal, 0, _characteristics.MaxHP);
        OnChangeHp();
    }

    public virtual void Spawn()
    {

    }

    public virtual void ManualAttack(Entity entity)
    {

    }

    public virtual void ManualMove(Vector3 vector3)
    {

    }

    public virtual void Select()
    {
        _selectCircle.gameObject.SetActive(true);
        _marker.gameObject.SetActive(true);
    }

    public virtual void Unselect()
    {
        _selectCircle.gameObject.SetActive(false);
        _marker.gameObject.SetActive(false);
    }

    public T Inst<T>() where T : Entity
    {
        var origin = _prefab ?? this;
        Entity entity = Pool.GetFromPool<Entity>(origin);
        entity._prefab = origin;
        return entity as T;
    }

    public void AddInPool()
    {
        gameObject.SetActive(false);
        transform.position = new Vector3(0, 50, 0);
        Pool.AddInPool(_prefab, this);
    }
}

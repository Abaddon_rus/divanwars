﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SpellInPanel : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField]
    Image _icon;

    [SerializeField]
    TextMeshProUGUI _key;

    [SerializeField]
    Button _button;

    [SerializeField]
    Button _buttonUP;

    [SerializeField]
    TimerCooldown _cooldown;

    KeyCode _keyCode;

    SpellSettings _spell;

    Hero _hero;

    private void Start()
    {
        _button.onClick.AddListener(Begin);
        _buttonUP.onClick.AddListener(Upgrade);
        _hero = Core.GetLink<Hero>();
        _buttonUP.gameObject.SetActive(_hero.FreePointSpell > 0 && _spell.Lvl < _spell.SpellLvls.Count);
    }

    private void Upgrade()
    {
        _hero.FreePointSpell--;
        _spell.Lvl++;
    }

    private void Begin()
    {
        if (_spell.Cooldown <= 0)
        {
            _spell.Begin();
            _spell.CurrentEffect.OnActive += Cooldown;
        }
    }

    public void Insta(SpellSettings spell, KeyCode keyCode)
    {
        _spell = spell;
        _icon.sprite = spell.Icon;
        _key.text = keyCode.ToString();
        _keyCode = keyCode;

        if (_spell.Cooldown > 0)
        {
            _cooldown.Cooldown(_spell.Cooldown);
        }
    }

    void Cooldown()
    {
        _spell.Cooldown = _spell.CurrentEffect.SpellParams.CooldownSec;

        ManualController mc = Core.GetLink<ManualController>();
        mc.StartCoroutine(mc.CooldownSpells(_spell));

        _cooldown.Cooldown(_spell.Cooldown);
        _spell.CurrentEffect.OnActive -= Cooldown;
    }

    private void Update()
    {
        _buttonUP.gameObject.SetActive(_hero.FreePointSpell > 0 && _spell.Lvl < _spell.SpellLvls.Count);

        if (_spell.Lvl == 0)
        {
            _button.interactable = false;
        }
        else
        {
            _button.interactable = true;
            if (Input.GetKeyDown(_keyCode))
            {
                Begin();
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        ManualController.UIFocus = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ManualController.UIFocus = false;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverPanel : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI _text;

    [SerializeField]
    Button _buttonReboot;

    private void Awake()
    {
        UIManager.GameOverPanel = this;
        transform.localPosition = new Vector3();
        gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        _buttonReboot.onClick.AddListener(Reboot);
    }

    private void Reboot()
    {
        Gold.Value = 0;
        Core.ClearPool();
        Pool.ClearPool();
        SceneManager.LoadScene(0);
    }

    public void GameOver(bool win)
    {
        if (win)
        {
            _text.text = "You won!";
        }
        else
        {
            _text.text = "You lose...";
        }
        gameObject.SetActive(true);
    }
}

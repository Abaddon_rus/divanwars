﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellsPlaceInPanel : MonoBehaviour
{
    [SerializeField]
    SpellInPanel _spellPrefab;

    [SerializeField]
    List<SpellInPanel> _spellInPanels;

    [SerializeField]
    List<KeyCode> _keyCodes;

    public void Insta(List<SpellSettings> spells)
    {
        for (int i = 0; i < _spellInPanels.Count; i++)
        {
            Destroy(_spellInPanels[i].gameObject);
        }

        _spellInPanels.Clear();

        for (int i = 0; i < spells.Count; i++)
        {
            SpellInPanel spellInPanel = Instantiate(_spellPrefab, transform);
            _spellInPanels.Add(spellInPanel);
            spellInPanel.Insta(spells[i], _keyCodes[i]);
        }
    }
}

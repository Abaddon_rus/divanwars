﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HeroPanel : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI _textLvl;

    [SerializeField]
    TextMeshProUGUI _textGold;

    [SerializeField]
    Image _fillHp;

    [SerializeField]
    Image _fillXp;

    [SerializeField]
    SpellsPlaceInPanel _spellPlace;

    public static Action<float> HP;
    public static Action<int> Gold;
    public static Action<float, int> ExpLvl;
    public static Action<Hero> Select;
    public static Action Unselect;

    private void Awake()
    {
        HP = ShowHP;
        Gold = ShowGold;
        ExpLvl = ShowExpLvl;
        Select = SelectHero;
        Unselect = UnselectHero;
        gameObject.SetActive(false);
    }

    public void SelectHero(Hero hero)
    {
        _spellPlace.Insta(hero._spells);
        ShowHP(hero.GetPercentHp());
        ShowExpLvl(hero.GetPercentExp(), hero._heroSettings.Lvl);
        gameObject.SetActive(true);
    }

    public void UnselectHero()
    {
        gameObject.SetActive(false);
    }

    void ShowHP(float hp)
    {
        _fillHp.fillAmount = hp;
    }

    void ShowGold(int gold)
    {
        _textGold.text = gold.ToString();
    }

    void ShowExpLvl(float exp, int lvl)
    {
        _textLvl.text = lvl.ToString();
        _fillXp.fillAmount = exp;
    }

}

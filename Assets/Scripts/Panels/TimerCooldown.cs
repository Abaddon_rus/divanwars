﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TimerCooldown : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI _seconds;

    int _sec;

    public void Cooldown(int sec)
    {
        _sec = sec;
        gameObject.SetActive(true);
    }

    IEnumerator Timer()
    {
        while (_sec > 0)
        {
            _seconds.text = _sec.ToString();
            yield return new WaitForSeconds(1);
            _sec--;
        }
        gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        StartCoroutine(Timer());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Gold
{
    static int gold = 0;

    public static int Value
    {
        set
        {
            gold = value;
            HeroPanel.Gold(gold);
        }
        get
        {
            return gold;
        }
    }
}

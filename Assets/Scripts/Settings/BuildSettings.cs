﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BuildSettings", menuName = "Settings/BuildSettings")]
public class BuildSettings : ScriptableObject
{
    public List<BuildPack> Levels;
}

[Serializable]
public class BuildPack
{
    public int PriceUpgrade;
    public int TimeOfCreation;
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "HeroSettings", menuName = "Settings/HeroSettings")]
public class HeroSettings : ScriptableObject
{
    public int Lvl;
    public BaseCharacteristics Characteristics;

    [HideInInspector]
    public List<LevelUpParams> LevelUpParams = new List<LevelUpParams>();
}

[Serializable]
public class LevelUpParams : BaseCharacteristics
{
    public int NeedExp = 100;
}


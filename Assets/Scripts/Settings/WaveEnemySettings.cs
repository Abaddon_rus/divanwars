﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WaveEnemySettings", menuName = "Settings/WaveEnemySettings")]
public class WaveEnemySettings : ScriptableObject
{
    public List<Wave> Wave;

    public Wave GetWave(string number) { return Wave.Find((x) => x.NumberWave == number); }
}


[Serializable]
public class Wave
{
    public string NumberWave;
    public int Melee;
    public int Range;
    public int Boss;
}

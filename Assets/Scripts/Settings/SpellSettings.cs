﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SpellSettings", menuName = "Settings/SpellSettings")]
public class SpellSettings : ScriptableObject
{
    public int Lvl = 0;

    public int Cooldown = 0;

    public Sprite Icon;

    public List<SpellParams> SpellLvls;

    public SpellEffect SpellEffect;

    public SpellEffect CurrentEffect;

    public void Begin()
    {
        if (CurrentEffect != null)
        {
            CurrentEffect.Cancel();
        }
        CurrentEffect = Instantiate(SpellEffect);
        CurrentEffect.SpellParams = SpellLvls[Lvl - 1];
        CurrentEffect.Begin();
        ManualController.UseSpells = true;
    }
}

[Serializable]
public class SpellParams
{
    public int CooldownSec;
    public int Radius;
    public int Damage;
    public float Slowdown;
    public float SlowdownAttack;
    public int TimeActionSec;
}

[Serializable]
public class SpellEffect : MonoBehaviour
{
    public Action OnActive = delegate { };

    public LayerMask Using;

    public SpellParams SpellParams;

    public virtual void Begin()
    {

    }

    public virtual void Activate()
    {
        OnActive();
    }

    public virtual void Cancel()
    {

    }
}

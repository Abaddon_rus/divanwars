﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "UnitSettings", menuName = "Settings/UnitSettings")]
public class UnitSettings : ScriptableObject
{
    public BaseCharacteristics Characteristics;
}

[Serializable]
public class BaseCharacteristics
{
    public int MaxHP = 0;
    public int Armor = 0;
    public int Attack = 0;
    public float AttackSpeed = 0;
    public int AttackRange = 0;
    public int Speed = 0;
    public int Gold = 0;
    public int Exp = 0;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Vector3 _shift;

    public static Hero SelectHero;

    private void Update()
    {
        if (SelectHero == null)
        {
            transform.position += _shift;
        }
        else
        {
            transform.position = new Vector3(SelectHero.transform.position.x, transform.position.y, SelectHero.transform.position.z - 2);
        }
    }
}

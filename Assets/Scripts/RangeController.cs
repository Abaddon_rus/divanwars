﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeController : MonoBehaviour
{
    public event Action<Entity> OnEnter = delegate { };

    public event Action<Entity> OnExit = delegate { };

    public List<Entity> Entity = new List<Entity>();

    private void OnTriggerEnter(Collider collider)
    {
        Entity entity = collider.gameObject.GetComponent<Entity>();
        if (entity != null)
        {
            Entity.Add(entity);
            entity.OnDie += TargetDie;
            OnEnter(entity);
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        Entity entity = collider.gameObject.GetComponent<Entity>();
        if (entity != null)
        {
            Entity.Remove(entity);
            entity.OnDie -= TargetDie;
            OnExit(entity);
        }
    }

    public T GetTarget<T>() where T : Entity
    {
        if (Entity.Count > 0)
        {
            for (int i = 0; i < Entity.Count; i++)
            {
                if (Entity[i]._mortal == false)
                {
                    return Entity[0] as T;
                }
            }
        }

        return null;
    }

    public void Clear()
    {
        Entity.Clear();
        OnEnter = delegate { };
        OnExit = delegate { };
    }

    public void TargetDie(Entity entity)
    {
        Entity.Remove(entity);
    }
}

﻿using UnityEngine;

public class Divan : Build
{
    void Awake()
    {
        Core.Stop = false;
        Core.Divan = transform;
    }

    public override void Die()
    {
        gameObject.SetActive(false);
        Core.Stop = true;
        UIManager.GameOverPanel.GameOver(false);
    }
}

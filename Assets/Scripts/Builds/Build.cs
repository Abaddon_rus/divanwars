﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Build : Entity, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    [SerializeField]
    int _maxHp = 10000;

    [SerializeField]
    HpBar _hpBar;

    private void Start()
    {
        _hp = _maxHp;
        OnChangeHp += ChangeHpBar;
    }

    void ChangeHpBar()
    {
        _hpBar.ChangeHp((float)_hp / _maxHp);
    }


    public void OnPointerClick(PointerEventData eventData)
    {

    }

    public void OnPointerEnter(PointerEventData eventData)
    {

    }

    public void OnPointerExit(PointerEventData eventData)
    {

    }
}

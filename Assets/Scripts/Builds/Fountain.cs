﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Fountain : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI _rebornTxt;

    [SerializeField]
    int _secndHeal = 1;

    [SerializeField]
    int _amountHealMinions = 10;

    [SerializeField]
    int _amountHealHero = 20;

    [SerializeField]
    int _secondRespawnHero = 30;

    [SerializeField]
    List<Entity> _entity = new List<Entity>();

    [SerializeField]
    Hero _hero;

    void Awake()
    {
        Core.AddLink(this);
    }

    private void Start()
    {
        SpawnHero();
        StartCoroutine(Healing());
    }

    private void OnTriggerEnter(Collider collider)
    {
        Entity entity = collider.gameObject.GetComponent<Entity>();
        if (entity != null)
        {
            _entity.Add(entity);
        }

        Hero hero = entity as Hero;
        if (hero != null)
        {
            hero._mortal = false;
            Core.RemoveMinion(entity);
            for (int i = 0; i < Core.Enemy.Count; i++)
            {
                Enemy enemy = Core.Enemy[i] as Enemy;
                enemy.EnemyUnitDie(hero);
            }
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        Entity entity = collider.gameObject.GetComponent<Entity>();
        if (entity != null)
        {
            _entity.Remove(entity);
        }

        if (entity as Hero)
        {
            entity._mortal = true;
            Core.AddMinion(entity);
        }
    }

    IEnumerator Healing()
    {
        while (true)
        {
            yield return new WaitForSeconds(_secndHeal);
            for (int i = 0; i < _entity.Count; i++)
            {
                if (_entity[i] as Minion)
                {
                    _entity[i].Heal(_amountHealMinions);
                }
                else if (_entity[i] as Hero)
                {
                    _entity[i].Heal(_amountHealHero);
                }
            }
        }
    }

    IEnumerator RespawnHero()
    {
        _rebornTxt.gameObject.SetActive(true);
        int i = _secondRespawnHero;
        while (i > 0)
        {
            _rebornTxt.text = string.Format("hero reborn after: {0} sec", i);
            i--;
            yield return new WaitForSeconds(1);
        }
        _rebornTxt.gameObject.SetActive(false);

        SpawnHero();
    }

    void SpawnHero()
    {
        Hero hero = _hero.Inst<Hero>();
        hero.transform.position = transform.position + new Vector3(0, 1, 0);
        hero.Spawn();
    }

    public void HeroDie()
    {
        StartCoroutine(RespawnHero());
    }
}
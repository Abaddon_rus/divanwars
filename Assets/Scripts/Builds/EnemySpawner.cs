﻿using System.Collections;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    Enemy _enemyMelee;
    [SerializeField]
    Enemy _enemyRange;
    [SerializeField]
    Enemy _enemyBoss;

    [SerializeField]
    WaveEnemySettings _waveEnemySettings;

    public static Coroutine _coroutine;

    int number = 1;

    private void Start()
    {
        StartCoroutine(Wait());
    }

    void SpawnWave(int numberWave)
    {
        Wave wave = _waveEnemySettings.GetWave(numberWave.ToString());
        for (int i = 0; i < wave.Melee; i++)
        {
            Spawn(_enemyMelee);
        }
        for (int i = 0; i < wave.Range; i++)
        {
            Spawn(_enemyRange);
        }
        for (int i = 0; i < wave.Boss; i++)
        {
            Spawn(_enemyBoss);
        }
        if (numberWave == _waveEnemySettings.Wave.Count && _coroutine == null)
        {
            _coroutine = StartCoroutine(GameOver());
        }
    }

    void Spawn(Enemy unit)
    {
        Enemy createdUnit = unit.Inst<Enemy>();
        createdUnit.transform.position = transform.position;
        createdUnit.name = unit.gameObject.name + " " + number.ToString();
        createdUnit.Spawn();
        number++;
    }

    IEnumerator Wait()
    {
        int numberWave = 1;
        while (numberWave <= _waveEnemySettings.Wave.Count && !Core.Stop)
        {
            yield return new WaitForSeconds(10);
            SpawnWave(numberWave);
            numberWave++;
        }
    }

    IEnumerator GameOver()
    {
        yield return null;

        while (Core.Enemy.Count > 0)
        {
            yield return null;
        }
        UIManager.GameOverPanel.GameOver(true);
    }
}

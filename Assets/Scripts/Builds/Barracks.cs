﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barracks : Build
{
    [SerializeField]
    Minion _unit;

    [SerializeField]
    Transform _spawnPoint;

    [SerializeField]
    BuildSettings _buildSettings;

    int _level = 1;

    private void Start()
    {
        StartCoroutine(SpawnUnits());
    }

    IEnumerator SpawnUnits()
    {
        yield return new WaitForSeconds(10);
        int i = 1;
        while (true && !Core.Stop)
        {
            Minion createdUnit = _unit.Inst<Minion>();
            createdUnit.transform.position = _spawnPoint.position;
            createdUnit.name = _unit.gameObject.name + " " + i.ToString();
            createdUnit.Spawn();
            yield return new WaitForSeconds(_buildSettings.Levels[_level - 1].TimeOfCreation);
            i++;
        }
    }

    public void Update()
    {
        
    }
}

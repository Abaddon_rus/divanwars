﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraMover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField]
    Vector3 _shift;

    [SerializeField]
    CameraController _cameraController;

    public void OnPointerEnter(PointerEventData eventData)
    {
        _cameraController._shift = _shift;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _cameraController._shift = new Vector3();
    }
}
